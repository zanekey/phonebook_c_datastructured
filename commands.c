#include "commands.h"

/**
 * Note this file is to contain console output,
 * meaning printf's are expected to be seen in this file.
 */

AddressBookList * commandLoad(char * fileName)
{ 
	 /* initialise variables */
	 
	 char fileInput[100];
	 char * token;
	 char stringID[6];
	 int ID;
	 char name[21];
	 FILE *fileReader;
	 AddressBookNode *node;
     AddressBookList *loadList;
	

	  
	  
	 /* open file*/
	 fileReader= fopen(fileName,"r");
	 printf("\n> Opening the file %s. \n",fileName);
	 
	 /* check file exists */
	 if ( fileReader == NULL )
	 {
		 printf("\n> Error: Unable to find the specified file.\n");
		 return NULL;
	 }
	 else /* load file it exists */
	 {	
	 	loadList= createAddressBookList();
		printf("> loading the file...\n");
		while (fgets(fileInput, 100, fileReader) != NULL)
		{
			if (fileInput[0] == '#') 
			{
				continue;
			}
			else
			{
				/* tokenize and obtain ID value from file */
				token = strtok(fileInput, ",");
				strcpy(stringID,token);
				ID = atoi(stringID);
				
				/* tokenize and obtrain name value from file */
				token = strtok(NULL,",");
				strcpy(name,token);		
				
				/*create reference node and pass in obtained values */
				node = createAddressBookNode(ID,name);
				/*pass node and list to insetNode function */
				insertNode(loadList,node);
	
				/* iterate through numbers and add to array */
				token = strtok(NULL , ",");
				while ( token != NULL)
				{
					addTelephone(node -> array , token);
					token = strtok(NULL , ",");
				}
				
			}
			
		}
		/* point the current pointer to the first item in the list */
		
		printf("> %d phone book entries have been loaded from the file.\n",loadList -> size);
		printf("> closing the file...\n");
		fclose(fileReader);
		return loadList;
	 }
	 
    return NULL;
}

void commandUnload(AddressBookList * list)
{
	/* free list */
	freeAddressBookList(list);
	printf("> The list is unloaded.\n");
}

void commandDisplay(AddressBookList * list)
{ 

	/* initialise Variables */
	int n , x;
	int largestString;
	int nameLength;
	char *name;
	AddressBookNode * tempNode;
	n = 0;
	largestString = 0;
	name = "Name";


	
	tempNode = list -> head;

	while ( list -> head !=NULL)
	{
		/* name length of current node */
		nameLength = strlen(list -> head -> name);
		
		if ( nameLength > largestString)
		{
			largestString = nameLength;
		}
		
		
		/* traverse into next node */
		list -> head = list -> head -> nextNode;
	}
	
	
	
 
	/* reset current pointer to head */
	list -> head = tempNode;
	
	/* print border of display */
	printf("\n---------------------------------------------\n");
	printf("| Pos |");
	printf(" Serial |");
	printf(" ID |");
	printf(" %-*s",largestString + 2, name);
	printf("| Telephones |\n");
	printf("---------------------------------------------\n");
	printf("---------------------------------------------\n");
	
	


	/* traverse through the doubly linked list and print node's in list */
	if ( list -> size != 0)
	{
	while ( list -> head !=NULL)
	{
		/* print current position of current pointer */
		if ( (list -> current) == (list -> head) )
		{
		 printf("| CUR ");
		}
		else
		{
			printf("|     ");
		}
		
		/* print serial */
		printf("|  %-*d" ,4,++n);
		/* print id */
		printf("|  %-*d",4,list -> head -> id);
		 
		
		if (list -> head -> name[strlen(list -> head -> name)-1]=='\n')
		{
			list -> head -> name[strlen(list -> head -> name)-1]='\0';
		}
		
		printf("|  %-*s|",largestString + 2,list -> head -> name);
		 
		
	
		/* check if node contains a telephone or not */
		if ( (list -> head -> array -> size) == 0 )
		{
			 printf("\n");
			 /* iterate to next node */
			 list -> head = list -> head -> nextNode;
			continue;
		}
		else
		{	
			/* print telephone numbers */
			for ( x = 0 ; x < (list -> head -> array -> size) ; x++)
			{
				if (list -> head->array->telephones[x][strlen(list -> head->array->telephones[x])-1]=='\n')
				{
					list -> head->array->telephones[x]
					[strlen(list -> head->array->telephones[x])-1]='\0';
						
				}
				/* multiple telephones or just one ? */
				if ( x == (list -> head -> array -> size))
				{
					printf(" %s", list -> head -> array -> telephones[x]);
				}
				else
				{
					printf(" %s,", list -> head -> array -> telephones[x]);
				}
				
				
				
			}
			printf("\n");
		
		}
		/* iterate to next node */
		 list -> head = list -> head -> nextNode;

	}
	}
	/* reset head to original location and print */
	 list -> head = tempNode;
	 printf("\n---------------------------------------------\n");
 	 printf("| Total phone book entries: %d \n", list -> size);
	 printf("---------------------------------------------\n");
	
}

void commandForward(AddressBookList * list, int moves)
{ 
	/* check if desired location is valid */
	if ( forward(list , moves))
	{
	 /* move current pointer to desired location */
	 int i;
	 for ( i = 0 ; i < moves ; i++)
	 {
		 list -> current = list -> current -> nextNode; 
	 }

	}
	else
	{
		printf("> desired position is out of bound.\n");
	}
}

void commandBackward(AddressBookList * list, int moves)
{ 
	int i;

	if (backward(list , moves)) /* check if desired position is valid */
	{
		/* move current pointer to specified posiiton */
		for ( i = 0 ; i < moves ; i++)
		 {
			list -> current = list -> current -> previousNode; 
		 }

	}
	else
	{
		printf("> desired position is out of bound.\n");
	}

}

void commandInsert(AddressBookList * list, int id, char * name, char * telephone)
{
	/* declare variables */
	AddressBookNode *node;
	
	/* check that the specified id is unique */
	if ( compareListNodesID(list,id))
	{
		/* insert specified details accordingly */
		node = createAddressBookNode(id,name);
		addTelephone(node -> array , telephone);
		insertNode(list,node);
	}
}

void commandAdd(AddressBookList * list, char * telephone)
{ 
	/* add telephone to where the list is pointing currently */
	addTelephone(list -> current -> array, telephone);
}

void commandFind(AddressBookList * list, char * name)
{ 
	/* declare and initialise variables */
	AddressBookNode *tempNode;
	tempNode = list -> head;

	
	/* traverse through list and search for specified name */
	while ( tempNode != NULL)
	{
		/* compare avaialble list names with specified name */
		if (strcmp(tempNode -> name,name)==0)
		{
			list -> current = tempNode;
			break;
		}
			
		tempNode  = tempNode -> nextNode;
		
		
	}

	/* if no name was found */
	printf("> unable to find node \n");


	
}

void commandDelete(AddressBookList * list)
{ }

void commandRemove(AddressBookList * list, char * telephone)
{ 

	removeTelephone(list -> current -> array,telephone);
}

void commandSort(
    AddressBookList * list,
    int sort(const void * node, const void * otherNode))
{
    /* Sort the nodes within list in ascending order using the
     * provided sort function to compare nodes.
     */
}


void commandSave(AddressBookList * list, char * fileName)
{
	/* initialise variables */
	FILE *fileWriter;
	AddressBookNode * tempNode;
	int i;
	
	/* save current position of current pointer */
	tempNode = list -> current;
	
	/* open file */
	fileWriter = fopen(fileName,"w");
	printf("> opening file %s \n",fileName);
	
	if ( fileWriter != NULL)
	{
		printf("> writing to list content to file... \n");
		/* traverse through list and write contents to file */
		while ( list -> current != NULL)
		{
			fprintf(fileWriter,"%d,",list -> current -> id);
			fprintf(fileWriter,"%s", list -> current -> name);
		
			for ( i = 0; i < list -> current -> array -> size ; i++)
			{
				fprintf(fileWriter,",%s",list -> current -> array -> telephones[i]);
			}
			
			fprintf(fileWriter,"\n");
			
		
			/* traverse list to next node */
			list -> current = list -> current -> nextNode;
		
		}
			
		/* close file */
		printf("closing file.\n");
		fclose(fileWriter);
	}
	else
	{
		printf("> file not created \n");
	}
	
	/* return current pointer to original position */
	list -> current = tempNode;
}

Boolean compareListNodesID(AddressBookList * list , int ID)
{
	

	AddressBookNode * tempNode;
	tempNode = list -> head;
	
	
	while ( tempNode !=NULL)
	{
		/* traverse through list and set nodes accordingly */
		
		if (tempNode -> id == ID)
		{
			printf("ID already exists\n");
			return FALSE;
		}
		/* traverse to next node */
		tempNode = tempNode -> nextNode;
	} 
	
	
	
	return TRUE;
	
	
	
}
