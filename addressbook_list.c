#include "addressbook_list.h"

/**
* Note this file is *NOT* to contain console output,
* meaning printf's are *NOT* to be present in this file.
*/

AddressBookList * createAddressBookList()
{
    /**
    * Allocates and initialises a new AddressBookList.
    * malloc or calloc are expected to be used to create the
    * AddressBookList memory.
    * 
    * If memory cannot be allocated for the list NULL is returned.
    * 
    * Note head, tail and current should all be initialised to NULL.
    */

	/*  create and allocate memory for list*/
	AddressBookList *addressBookList = malloc(sizeof(*addressBookList));

	/* set list pointers */	
	if (addressBookList != NULL)
	{
		addressBookList -> head = NULL;
		addressBookList -> tail = NULL;
		addressBookList -> current = addressBookList -> head;
		addressBookList -> size = 0;
		addressBookList -> serial  = 1;
		return addressBookList;
	}
	
	return NULL;

}

void freeAddressBookList(AddressBookList * list)
{
    /**
     * Free's all nodes within the list and the AddressBookList itself.
     * 
     * Note the freeAddressBookNode(...) function is used to free a node.
     */
	  
	  list -> current = list -> head;

	  
   while (list -> current!= NULL)
    {
       freeAddressBookNode(list->current);
	   list -> current = list -> current -> nextNode;
    }
	

	list -> size = 0;
	free(list);
}

AddressBookNode * createAddressBookNode(int id, char * name)
{
    /**
    * Allocates and initialises a new AddressBookNode.
    * malloc or calloc are expected to be used to create the
    * AddressBookNode memory.
    * 
    * Note the array of the node should also be created and initialised,
    * which should be done with the createAddressBookArray(...) function.
    * 
    * If memory cannot be allocated for the node or array NULL is returned.
    * 
    * Note previousNode and nextNode should both be initialised to NULL.
    */

    /* my code starts here ***
     create new address book node. */

	AddressBookNode * node = malloc(sizeof(*node));

 
	
	if ( node == NULL)
	{
		return NULL;
	}
	else
	{
		node -> id = id;
		strcpy(node -> name , name); 
		node -> nextNode  = NULL;
		node -> previousNode = NULL;
		node -> array = createAddressBookArray();
		return node;
	}
   
  
}

void freeAddressBookNode(AddressBookNode * node)
{
    /**
    * Free's the array within the node and the AddressBookNode itself.
    * 
    * Note the freeAddressBookArray(...) function is used to free the array.
    */
	
	freeAddressBookArray(node->array);
	
	free(node);
	
}

Boolean insertNode(AddressBookList * list, AddressBookNode * node)
{
  

	 /* if no current elements in the list */
	 if ( list -> head == NULL)
	 {
		 /* set nodes accordingly */
		 list -> head = node;
		 list -> current = node;
		 list -> tail = node;
		 list -> size += 1;
		 return TRUE;
	 }
	 else /* if list is not empty */
	 {
		/* set nodes appropiately , and traverse to next node */
          node->previousNode=list->tail;
		  list->tail->nextNode=node; 
		  list->tail=node;
		  list -> size += 1;
		  return TRUE;

	 }

}

Boolean deleteCurrentNode(AddressBookList * list)
{
    /**
     * Delete's and free's the current node in the list and returns TRUE.
     * 
     * Note the freeAddressBookNode(...) function is used to free a node.
     * 
     * If the list has no nodes (i.e., there is no current node)
     * then FALSE is returned.
     */
    
    return FALSE;
}

Boolean forward(AddressBookList * list, int forward)
{
    /**
     * Moves the current node forward in the list by the number provided
     * and returns TRUE.
     * 
     * If the current node cannot be moved forward by that many positions
     * then FALSE is returned and current remains unchanged.
     */
	 
	 /* declare and initialise variable */
	int nextPositionValue;
	nextPositionValue = (list -> serial) + forward;
	
	/* check if required position is invalid(out of bounds), return false */
	 if ( nextPositionValue >= (list-> size) )
	 {
		 return FALSE;
	 }/* if within bounds , return true */
	 else
	 {
		 list -> serial = nextPositionValue;
		 return TRUE;
	 }
	 
}

Boolean backward(AddressBookList * list, int backward)
{
    /**
    * Moves the current node backward in the list by the number provided
    * and returns TRUE.
    * 
    * If the current node cannot be moved backward by that many positions
    * then FALSE is returned and current remains unchanged.
    */

	/* decalare and initialise variable */
	int nextPositionValue;
	nextPositionValue = (list -> serial) - backward;
	
	/* check if requried location is out of bounds , return false */
	 if ( nextPositionValue < 0 )
	 {

		 return FALSE;
	 }
	 else /* if required location is valid return true */
	 {
		 list -> serial = nextPositionValue;
		 return TRUE;
	 }
	
	
	
 
}

AddressBookNode * findByID(AddressBookList * list, int id)
{
    /**
     * Returns the node that matches the id provided.
     * 
     * If no node with a matching id exists then NULL is returned.
     */

    return NULL;
}

AddressBookNode * findByName(AddressBookList * list, char * name)
{
    /**
    * Sets current to the first node that matches the name provided
    * and returns this node.
    * 
    * If no node with a matching name exists then NULL is returned
    * and current remains unchanged.
    */

    return NULL;
}
