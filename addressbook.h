#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include "commands.h"


void processInput();
AddressBookList * commandLineInput(char * fileName);
AddressBookList * normalInput ();
void displayInfo();
#endif
