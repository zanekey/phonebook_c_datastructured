#include "addressbook.h"

/**
* This file is to contain the main menu loop and thus it will have console
* output, meaning printf's are expected to be seen in this file.
* 
* After the user enters a command, then the corresponding function is to be
* called from commands.c, e.g., "forward" corresponds to "commandForward(...)".
*/





int main(int argc, char * argv[])
{
	/* initialise and declare variables */
	AddressBookList * list;

	/* call print details */
	displayInfo();
	
	/* identify if command line entry */
	if ( argc == 2 ) /* is input from command line? */
	{
		list = commandLineInput(argv[1]);
	}
	else /* user input from within program */
	{
		list = normalInput();
	}
	
	
	/* call process input */
	processInput(list);
	
	
    printf("> Goodbye. \n\n");

	
    return EXIT_SUCCESS;
}

AddressBookList * commandLineInput(char * fileName)
{

	AddressBookList * list;
	printf("filename = %s \n",fileName);
	list = commandLoad(fileName);
	return list;

	
}

AddressBookList * normalInput ()
{
	/* initialise variables */
	char userInput[21];
	char userChoice[5];
	char * token;
	AddressBookList * list;

	
	while (TRUE)
	{
		printf("Enter your command: ");
		fgets(userInput, 80, stdin);
		if(userInput[strlen(userInput) - 1] != '\n')
			{
				printf("Invalid input. \n");
				readRestOfLine();
			
			}
		userInput[strlen(userInput) - 1] = '\0';
			
		/* tokenize first input of user */	
		token = strtok(userInput," ");
			
		/* if the input is load */
		if(strcmp(userInput, COMMAND_LOAD) == 0)
		{
			token = strtok (NULL ,"\n");
			strcpy(userChoice,token);
			list = commandLoad(userChoice);
			return list;
		}
		else if(strcmp(userInput, COMMAND_QUIT) == 0)
		{ 
		
			printf("> Goodbye. \n\n");
			exit(0);
		}	
		else
		{	
			continue;
		}
	}

}

void processInput(AddressBookList * list)
{
	/* initialise variables */
	char userInput[21];
	char userChoice[5];
	char * token;
	int val;

	while(TRUE)
	{
	/* prompt user for input */
	printf("Enter your command: ");
	
	fgets(userInput, 80, stdin);
	if(userInput[strlen(userInput) - 1] != '\n')
		{
			printf("> Invalid input. \n");
			readRestOfLine();
		
		}
	userInput[strlen(userInput) - 1] = '\0';
		
	
	/* tokenize first input of user */	
	token = strtok(userInput," ");	
	
	/* if the input is load */
	if(strcmp(userInput, COMMAND_LOAD) == 0)
	{
		token = strtok (NULL ,"\n");
		strcpy(userChoice,token);
		list = commandLoad(userChoice); 
	}
	
	/* if the input is display */
	if(strcmp(userInput, COMMAND_DISPLAY) == 0)
	{
	
		commandDisplay(list); 
	}
	
	/* if the input is unload */
	if(strcmp(userInput, COMMAND_UNLOAD) == 0)
	{
		commandUnload(list);

	}
	
	/* if the input is quit */
	if(strcmp(userInput, COMMAND_QUIT) == 0)
	{
		commandUnload(list); 
		break;
	}
	
	/* if the input is forward */
	if(strcmp(userInput, COMMAND_FORWARD) == 0)
	{
		token = strtok (NULL ,"\n");
		strcpy(userChoice,token);
		val = atoi(userChoice);
		commandForward(list,val);
	}
	
	/* if the input is backward */
	if(strcmp(userInput, COMMAND_BACKWARD) == 0)
	{
		token = strtok (NULL ,"\n");
		strcpy(userChoice,token);
		val = atoi(userChoice);
		commandBackward(list,val);
	}
	
	/* if the command is insert */
	if(strcmp(userInput, COMMAND_INSERT) == 0)
	{
		/* declare initialise variables */
		int i;
		int ID;
		char * endPtr, * name, * telephone;
		telephone = NULL;
		
		token = strtok (NULL ,",");
				
		/* convert to integer */
		ID = strtol(token, &endPtr, 10);
		if(endPtr == token || *endPtr != '\0')
		{
			printf("please input id\n");
			continue;
		}
		
		
	
		/* tokenise name input */
		token = strtok (NULL ,",");
		name = token;
		
		/* validate name has been entered */
		if (name == NULL)
		{
			printf("please enter a name\n");
			continue;
		}
		
		
		/* tokenize telephone input */

		token = strtok (NULL, "");
		strcpy(telephone,token);
		
		/* validate appropiate length of telephone */
		if(strlen(telephone) !=  TELEPHONE_LENGTH - NULL_SPACE)
		{
			printf("> telephone must consist of 10 digits\n");
			continue;
		}
		
		/* make sure telephone consists of numeric values only */
	
		for ( i = 0 ; i < strlen(telephone); i++)
		{
			/* validate ID input and verify it is numeric */
			if(isdigit(telephone[i]) == 0)
			{
				printf("> Telephone must consist of numeric values only\n");
				continue;
			}
		}
		commandInsert(list,ID,name,telephone);
	}
	
	/* command remove called */
	if(strcmp(userInput, COMMAND_REMOVE) == 0)
	{
		token = strtok (NULL, " ");
		commandRemove(list,token); 
	}
	
	/* command find called */
	if(strcmp(userInput, COMMAND_FIND) == 0)
	{
		token = strtok (NULL, " ");
		commandFind(list,token); 
	}
	
	if(strcmp(userInput, COMMAND_ADD) == 0)
	{
		/* initialise variables */
		int i;
		token = strtok (NULL, "");
	
		
		/* validate appropiate length of telephone */
		if(strlen(token) !=  TELEPHONE_LENGTH - NULL_SPACE)
		{
			printf("telephone must consist of 10 digits\n");
			continue;
		}
		

		
		/* make sure telephone consists of numeric values only */
	
		for ( i = 0 ; i < strlen(token); i++)
		{
			/* validate ID input and verify it is numeric */
			if(isdigit(token[i]) == 0)
			{
				printf("Telephone must consist of numeric values only\n");
				continue;
			}
		}
		
		commandAdd(list,token);
		
		
		
	}
	
	/* save commmand */
	if(strcmp(userInput, COMMAND_SAVE) == 0)
	{	
		token = strtok (NULL, "");
		commandSave(list,token);	
	}
	
	
	
	}

}

void displayInfo()
{
	printf("Name: %s \n" , STUDENT_NAME);
    printf("No: %s \n", STUDENT_ID);
    printf("Email: %s \n", SUBJECT);
	printf("-------------------- \n");
	
}

