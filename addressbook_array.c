#include "addressbook_array.h"

/**
* Note this file is NOT to contain console output,
* meaning printf's are NOT to be present in this file.
*/

AddressBookArray * createAddressBookArray()
{
	/* declare and initialise variable */
	AddressBookArray * addressBookArray;
	addressBookArray= malloc(sizeof(*addressBookArray));
	 
	
	if ( addressBookArray == NULL) /* make sure memory has been allocated */
	{
		return NULL;
	}
	else
	{
		/* create array */
		addressBookArray -> size = 0; 	
		addressBookArray -> telephones = NULL;
		return addressBookArray;
	}


}

void freeAddressBookArray(AddressBookArray * array)
{
	/* initialise variable */
	 int i;
	 
	 

	/* iterate through list and remove telephone */
	 for ( i = 0 ; i < array -> size ; i++)
	 {
		 free(array -> telephones[i]);
	 }
	 
	/*free array mempory */
	 free(array);
	 
	 
}

Boolean addTelephone(AddressBookArray * array, char * telephone)
{
	/* declare and initialise variables */
	char * newTelephone;
	newTelephone= malloc(TELEPHONE_LENGTH - NULL_SPACE);
	
	
    strcpy(newTelephone, telephone);
	
	/* reallocate memory to array */
	array->telephones = realloc(array->telephones,
    sizeof(*array->telephones) * (array->size + 1));
	
	/* add telephone to array and increment size */
	array->telephones[array -> size] = newTelephone;
	array->size++;
	return TRUE;

}

Boolean removeTelephone(AddressBookArray * array, char * telephone)
{

	

	/* declare and initialise variables */
	int removeIndex, i;
	AddressBookArray * temp;

	
	
	
	/* i to be used for index of element to be removed */
	removeIndex = -1;
	
	/* check array contains telephones */
	if( array -> telephones == NULL)
	{
		return FALSE;
	}
	
	/* find telephone to be deleted and acquire index */
	for ( i = 0; i < array -> size; i++)
	{
		if (strcmp(array->telephones[i],telephone) == 0)
		{
			removeIndex = i;
			break;
		}
	}
	
	/* if specified telephone does not exist */
	if( removeIndex == -1)
	{
		return FALSE;
	}
		
	/* step 2 moving the elemenent to be deleted to the end */
	temp = array -> telephones[removeIndex];
	array ->telephones[removeIndex] = array -> telephones[array -> size -1];
	array -> telephones[array -> size -1] = temp;
	
	/* if only one telephone exists in array */
	if ( array -> size == 1)
	{
		free(array -> telephones);
		array -> telephones = NULL;
	}
	else  /* reallocate memory */
	{
	 array->telephones = realloc(array->telephones,
         sizeof(*array->telephones) * (array->size - 1));
					   
	}
		
	/* decrement array size and free temp */
    array->size--;
	free(temp);

    return TRUE;
}

char * findTelephone(AddressBookArray * array, char * telephone)
{
   

    return NULL;
}
